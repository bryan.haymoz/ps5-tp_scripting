# Automated Deployement of VM's

## Informations générales

* Etudiant : Bryan Haymoz
* Superviseur : Patrick Gaillet
* Co-superviseur : François Buntschu
* Dates : du 27.09.2021 au 09.02.2022

## Description

Le but de ce projet est de fournir une méthode d’automatisation de déploiement de VM’s ou de service dans le cadre d’un use case préalable. 

Ce use case est un futur travail pratique dont le but sera de mettre en place un cluster Kubernetes par-dessus cette solution.

Les principaux objectifs de ce projet sont :

* Détermination des configurations nécessaires pour le déploiement et la configuration automatisés des machines virtuelles.
* Sélection et conception de la méthode la plus optimale pour pouvoir automatiser le déploiement et les configurations des machines virtuelles via les API’s d’OpenStack.
* Conception d’une maquette permettant le déploiement automatisé de machines virtuelles et leur configuration sur OpenStack et mise à disposition de celle-ci à la fin du projet.
